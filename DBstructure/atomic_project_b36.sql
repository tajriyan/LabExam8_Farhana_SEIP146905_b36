-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2016 at 06:38 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `birth_day` date NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `name`, `birth_day`, `is_deleted`) VALUES
(1, 'Sharmin', '1985-05-20', 'no'),
(2, 'dd', '1985-05-25', 'no'),
(3, 'Nafisa', '0000-00-00', '2016-11-19 22:30:50'),
(4, 'farahah', '1955-05-20', 'no'),
(5, 'rehan', '1960-03-09', 'no'),
(6, 'shanta', '1965-04-02', 'no'),
(7, 'rima', '1985-02-03', 'no'),
(8, 'hero', '1985-02-03', 'no'),
(9, 'jamal', '1982-02-13', 'no'),
(10, 'jibon', '1985-05-20', 'no'),
(11, 'titu', '1982-02-13', 'no'),
(12, 'yakub', '1985-05-20', 'no'),
(14, 'Jahangir', '1985-05-20', 'no'),
(15, 'lili', '1985-02-03', 'no'),
(16, 'pinky', '1985-05-25', 'no'),
(17, 'kabery', '1985-05-20', 'no'),
(18, 'papri', '1982-02-13', 'no'),
(19, 'Nibraz', '0000-00-00', 'no'),
(20, 'cccc', '1982-07-15', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(6, 'Chashi', 'Jashim', 'no'),
(7, 'Srikanta', 'Sharat Chandra', 'no'),
(8, 'Nil Akash', 'Rabi', '2016-11-19 22:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `city_name` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `is_deleted`) VALUES
(1, 'shahjahan', 'Rajshahi', 'no'),
(2, 'ddd', 'Dhaka', 'no'),
(3, 'fdsfsd', 'Ctg', '2016-11-19 22:43:09'),
(6, 'fiza', 'Barishal', 'no'),
(7, 'ddd', 'Rajshahi', 'no'),
(9, 'Rima', ' Khulna', 'no'),
(10, 'rabeya', 'Rajshahi', 'no'),
(11, 'Sihab', 'Barishal', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email_address` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email_address`, `is_deleted`) VALUES
(1, 'daina', 'far@yahoo.com', 'no'),
(2, 'saddam', 'saddam.jbl@yahoo.com', 'no'),
(3, 'shahana', 'shah@gmail.com', 'no'),
(4, 'Tina', 'tina@yahoo.com', 'no'),
(6, 'Hamida', 'hamida@gmail.com', 'no'),
(7, 'shamim', 'shamim.hassan@gmail.com', 'no'),
(8, 'faa', 'saddam.jbl@yahoo.com', 'no'),
(9, 'Farheen', 'shah@gmail.com', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender_name` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender_name`, `is_deleted`) VALUES
(1, 'fff', 'female', '2016-11-19 22:43:48'),
(3, 'Farhan', 'male', 'no'),
(5, 'jahan', 'female', 'no'),
(6, 'Saddam', 'male', 'no'),
(7, 'Salam', 'male', 'no'),
(8, 'Ayesha', 'female', 'no'),
(9, 'xxxx', 'female', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobby_name` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby_name`, `is_deleted`) VALUES
(14, 'Jana', 'Drawing,Gardening,Dancing', '2016-11-19 22:44:40'),
(15, 'ssss', 'Gardening,Singing,', 'no'),
(16, 'Niha', 'Drawing,Gardening,Singing,Dancing', 'no'),
(17, 'karim', 'Gardening,Singing,', 'no'),
(18, 'hamid', 'Drawing,Dancing,', 'no'),
(30, 'Sneha', 'Drawing,Gardening', 'no'),
(33, 'Habiba', 'Gardening,Singing,Dancing', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE `profile_pic` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `profile_pic` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pic`
--

INSERT INTO `profile_pic` (`id`, `name`, `profile_pic`, `is_deleted`) VALUES
(24, 'Jalal', '/xampp/htdocs/Farhana_SEIP146905_b36_Session27/image/1479573985hobby1.jpg', '2016-11-19 22:46:29'),
(39, 'dalia', '/xampp/htdocs/Farhana_SEIP146905_b36_Session27/image/1479573936city6.jpg', 'no'),
(41, 'Monsur', '/xampp/htdocs/Labexam7_Farhana_SEIP146905_b36/image/1479487583book.jpg', 'no'),
(42, 'Baby', '/xampp/htdocs/Labexam7_Farhana_SEIP146905_b36/image/1479487693book.jpg', 'no'),
(47, 'Yaseen', '/xampp/htdocs/Farhana_SEIP146905_b36_Session27/image/1479493356city6.jpg', 'no'),
(48, 'Sadaa', '/xampp/htdocs/Farhana_SEIP146905_b36_Session27/image/1479493545birt.png', 'no'),
(50, 'Shayla', '/xampp/htdocs/LabExam8_Farhana_SEIP146905_b36/image/1479576643birthday.jpg', '2016-11-19 23:30:51');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `company_name` varchar(111) NOT NULL,
  `company_summary` varchar(111) NOT NULL,
  `is_deleted` varchar(1000) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `company_name`, `company_summary`, `is_deleted`) VALUES
(3, 'DeshIT Ltd.', 'Its a IT firm', 'no'),
(4, 'MGH Group', 'Multinational Company. It has many Branches in Bangladesh', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
