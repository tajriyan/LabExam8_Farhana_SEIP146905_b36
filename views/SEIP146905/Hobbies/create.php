

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/asset/font-awesome/4.2.0/css/font-awesome.min.css">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        .inputstl {
            padding: 9px;
            border: solid 1px #460023;
            outline: 0;
            background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #FFCEE7), to(#FFFFFF));
            background: -moz-linear-gradient(top, #FFFFFF, #FFCEE7 1px, #FFFFFF 25px);
            box-shadow: rgba(0,0,0, 0.1) 10px 5px 8px;
            -moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
            -webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;

        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p").delay(2000).fadeOut("slow")
        });
    </script>

</head>

<body>
<p>
<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
echo Message::message();
?></p>
<div class="container">
    <h1>Hobby Selection</h1>
    <form class="form-horizontal" role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="name1" class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-4">
                <input type="text" class="form-control inputstl" name="name" placeholder="Enter Your Full Name">
            </div>
        </div>
        <div class="form-group">

        <div class="form-group">
            <label for="Email1msg" class="col-sm-2 control-label">Your Hobby:</label>
            <div class="col-sm-5">

                <div class="checkbox checkbox-success">
                    <input id="chkphp" class="styled" type="checkbox" name="hobby_name[]" value="Drawing" >
                    <label for="chkphp">
                        Drawing
                    </label>
                </div>
                <div class="checkbox checkbox-success">
                    <input id="chkcss" class="styled" type="checkbox" name="hobby_name[]" value="Gardening" >
                    <label for="chkcss">
                        Gardening
                    </label>
                </div>
                <div class="checkbox checkbox-warning">
                    <input id="chkhtml" class="styled" type="checkbox" name="hobby_name[]" value="Singing" >
                    <label for="chkhtml">
                        Singing
                    </label>
                </div>
                <div class="checkbox checkbox-danger">
                    <input id="chkboot" class="styled" type="checkbox" name="hobby_name[]" value="Dancing" >
                    <label for="chkboot">
                        Dancing
                    </label>

            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-4">
                <button type="submit" class="btn btn-lg btn-block btn-danger">Create</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>



