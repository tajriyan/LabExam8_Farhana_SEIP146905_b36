<head>
    <link href="../../../resource/asset/css/modern-business.css" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/asset/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/asset/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/asset/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    <script language="JavaScript">
        function ConfirmDelete() {
            return confirm("Are you sure you want to delete?");
        }
    </script>
    <!-- Bootstrap Core CSS -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('../../../image/book.jpg');"></div>
            <div class="carousel-caption">
                <h2>Book Title</h2>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('../../../image/a.jpg');"></div>
            <div class="carousel-caption">
                <h2>Book Title</h2>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Slide Three');"></div>
            <div class="carousel-caption">
                <h2>Book Title</h2>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Atomic Project</a>
        </div>
        <br>

        <?php

        require_once("../../../vendor/autoload.php");
        use App\ProfilePicture\ProfilePicture;
        use App\Message\Message;


        $objBookTitle = new ProfilePicture();
        $objBookTitle->setData($_GET);
        $allData = $objBookTitle->index();
        $serial = 1;
        echo "<table class='table table-bordered table-striped table-hover' style='width: 800px'>";
echo "<caption >List Of Book Title </caption>";
        echo "<th class='info'> Serial </th>";
        echo "<th class='warning'> ID </th>";
        echo "<th class='success'> Name</th>";
        echo "<th class='active'> Profile Picture </th>";
       echo "<th class='info'> Action </th>";


     foreach($allData as $oneData){

            echo "<tr  class='danger'style='height: 40px'>";
            echo "<td>".$serial."</td>";
            echo "<td>".$oneData->id."</td>";
            echo "<td>".$oneData->name."</td>";
           $path=$oneData->profile_pic;
         $path1=str_replace("xampp/htdocs","../../../..",$path);


           echo "<td>" .'<img src="'.$path1.' " />'. "</td>";
            echo "<td>";

            echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
            echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
         echo "<a href='trash.php?id=$oneData->id'><button class='btn btn-success' >Trash</button></a> ";
            echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger' Onclick='return ConfirmDelete()' >Delete</button></a> ";


            echo "</td>";

            echo "</tr>";

            $serial++;
        }

        echo "</table>";

        ?>
    </nav>
</div>
</body>
</html>