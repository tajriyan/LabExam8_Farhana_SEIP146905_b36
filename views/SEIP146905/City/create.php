<!DOCTYPE html>
<head>

    <title>Bootstrap Login Form Template</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p").delay(2000).fadeOut("slow")
        });
    </script>
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/asset/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/asset/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/asset/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/asset/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>
<p><?php
    require_once("../../../vendor/autoload.php");
    use App\Message\Message;
    echo Message::message();
    ?>
</p>
<div class="top-content">

    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Select Your City</h3>
                    <h4>Enter Name And City Name:</h4>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-building"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="store.php" method="post" class="login-form">
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Name</label>
                        <input type="text" name="name" placeholder="Name..." class="form-username form-control" id="book_title">
                    </div>
                    <div class="form-group">
                        <label>City Name:</label><br>
                        <select name="city_name[]">
                            <option value="Ctg">Ctg</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Khulna">Khulna</option>
                        </select>
                    </div>
                    <button type="submit" class="btn">Create</button>
                </form>
            </div>
        </div>

    </div>


</body>
</html>