<?php
namespace App\Message;

if(!isset( $_SESSION))
    session_start();


class Message{
    public static function message($message=NULL)
    {
        if (is_null($message)) {
            $_message = self::getMessage();
            return $_message;
        } else {
            self::setMessage($message);
        }
    }

    public static function setMessage($message){
           $_SESSION['message']=$message;

        }
    public static function getMessage(){
           if(isset($_SESSION['message'])) $_message= $_SESSION['message'];//$_message is a local variable which will hold the message of session variable
           else $_message='';

         $_SESSION['message']="";// if reload then previous message will be delated
            return $_message;

        }


}