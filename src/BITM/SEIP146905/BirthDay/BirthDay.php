<?php
namespace App\BirthDay;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
class BirthDay extends DB{
    public $id;
    public $name;
    public $birth_day;
    public function __construct()
    {
        parent:: __construct();
    }
    public function setData($postVariable=null)
    {

        if(array_key_exists("id",$postVariable))
        {
            $this->id =        $postVariable['id'];
        }
        if(array_key_exists("name",$postVariable))
        {
            $this->name =        $postVariable['name'];
        }
        if(array_key_exists("birth_day",$postVariable))
        {
            $this->birth_day =        $postVariable['birth_day'];
        }
    }
    public function store()
    {
        $arrayData=array($this->name,$this->birth_day);
        $sql="insert into birthday(name,birth_day)VALUES (?,?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrayData);
        if($result)
            Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->conn->query("SELECT * from birth_day where is_deleted='no'");

            $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from birth_day where id='.$this->id;

        $STH = $this->conn->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update()
    {
        $arrayData=array($this->name,$this->birth_day);
        $sql="update birth_day set name=?, birth_day=? where id=".$this->id;
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrayData);

        Utility::redirect('index.php');

    }// end of update()
    public function delete(){

        $sql = "Delete from birth_day where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()
    public function trash(){

        $sql = "Update birth_day SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()


}
?>

