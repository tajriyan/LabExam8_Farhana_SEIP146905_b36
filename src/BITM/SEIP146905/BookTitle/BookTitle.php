<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
    {
        parent:: __construct();

    }
    public function setData($postVariable=null)
    {

       if(array_key_exists("id",$postVariable))
       {
        $this->id =        $postVariable['id'];
       }
        if(array_key_exists("book_title",$postVariable))
        {
            $this->book_title =        $postVariable['book_title'];
        }
        if(array_key_exists("author_name",$postVariable))
        {
            $this->author_name =        $postVariable['author_name'];
        }
    }
    public function store()
    {
        $arrayData=array($this->book_title,$this->author_name);
        $sql="insert into book_title(book_title,author_name)VALUES (?,?)";
       $STH= $this->conn->prepare($sql);
       $result= $STH->execute($arrayData);
        if($result)
        Message::message("data has been inserted successfully");
        else
            Message::message("Failure ....Data is not inserted");
        Utility::redirect('create.php');
    }
    public  function index()
    {
        $STH = $this->conn->query("SELECT * from book_title where is_deleted='no'");

            $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->conn->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }
    public function update()
    {
        $arrData = array ($this->book_title, $this->author_name);
      $sql="update book_title set book_title=?, author_name=? where id=".$this->id;
        $STH = $this->conn->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()
    public function delete(){

        $sql = "Delete from book_title where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()
    public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->conn->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

}
?>

